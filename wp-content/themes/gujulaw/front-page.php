<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


<div class="banner" style="background: url('<?php the_field('banner_image');?>') no-repeat center top;">
        <div class="container">
             <div class="header_description">
             <?php the_field("banner_content");?>
             <a href="<?php the_field("banner_button_link");?>" class="learn_more_btn" title="<?php the_field("banner_button_text");?>"><?php the_field("banner_button_text");?></a>
             </div>
        </div>
     </div>


  <!--<div class="logo_sec">
  	 <div class="container">
  			<div class="logo_block">
  		    <ul>    
             <?php if( have_rows('home_logo') ):
                  while ( have_rows('home_logo') ) : the_row(); ?>
                  <li>
                      <figure>
                          <a href="<?php echo site_url(); ?>">
                          	<?php $home_logo = get_sub_field('home_logo_image'); ?>
                              <img src="<?php echo $home_logo['url']; ?>"  alt="<?php echo $home_logo['alt']; ?>" title="<?php echo $home_logo['title']; ?>"/>
                          </a>
                      </figure>
                  </li>
                <?php endwhile;
              endif; ?>
          </ul>
        </div>
      </div>
  </div>-->

       <div class="welcome_sec">
       	  <div class="container">
         		<div class="welcome_title">
         			<h2><?php the_field('welcome_title'); ?></h2>
         		</div>
         		<div class="welcome_para">
         			<p><?php the_field('welcome_para'); ?></p>
         		</div>

           		<div class="welcome_image">
           			  <ul>
           				<?php if( have_rows('welcome_image') ):
                    	while ( have_rows('welcome_image') ) : the_row(); ?>
                    	<li>
                    		<div class="welcome_img_block">
    	                        <figure>
    	                            <a href="#">
    	                            	<?php 
    	                            	$welcome_video = get_sub_field("welcome_video");
    	                            	$welcome_img = get_sub_field('welcome_image1'); 

    	                            	if($welcome_video !="") {
    	                            	?>
    	                            	<iframe src="https://www.youtube.com/embed/<?php echo $welcome_video; ?>" frameborder="0" gesture="media" allowfullscreen></iframe>
    	                            	<?php } else { ?>
    	                                <img src="<?php echo $welcome_img['url']; ?>"  alt="<?php echo $welcome_img['alt']; ?>" title="<?php echo $welcome_img['title']; ?>"/>
    	                                <?php } ?>
    	                            </a>
    	                        </figure>
                        	</div>
                            <div class="welcome_img_text">
                            	<p><?php the_sub_field('welcome_imgtext'); ?></p>
                            </div>
                            <div class="welcome_img_link">
                            	<?php if($welcome_video !="") {
    	                            	?>
                            	<a href="<?php the_sub_field('welcome_link'); ?>" target="_blank">Watch Now</a>
                            	<?php } else { ?>
                            	<a href="<?php the_sub_field('welcome_link'); ?>">Learn More</a>
                            	<?php } ?>
                            </div>
                    	</li>
    		               <?php endwhile;
    		           		endif; ?>
           			</ul>
           		</div>
		      </div>
     </div>

     <div class="event_sec">
       <div class="container">
         <div class="event_title">
           <h2><?php the_field('event_title'); ?></h2>
         </div>
         <!--<ul class="event_list owl-carousel">-->
		 <ul>
            <?php
              $recent_posts = wp_get_recent_posts(array('numberposts'=>3, 'sattus'=>"publish")); 
              $i=1;
              foreach($recent_posts as $recent){ ?>
              <li>
                <div class="event_imgbox"><figure><a href="<?php echo get_permalink($recent["ID"]); ?>"> <?php $img = get_the_post_thumbnail($recent['ID'], 'full'); if($img != ''){echo $img;  } else{?> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog1.png"> <?php } ?></a></figure></div>
            <div class="event_description">
            <h4><a href="<?php echo get_permalink($recent["ID"]); ?>"><?php echo $recent["post_title"] ?></a></h4>
            <div class="event-content"><?php echo wp_trim_words($recent["post_content"], 16, '...'); ?></div>
            </div>
            </li>
            <?php  $i++; }
            ?>
        </ul>
          <div class="event_btn">
              <a href="<?php echo site_url(); ?>/blog">View all events</a>
          </div>
       </div>
     </div>


<?php get_footer();

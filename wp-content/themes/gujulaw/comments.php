<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="add-comment">

    <?php if ( have_comments() ) : ?>
        

        <?php //twentyfifteen_comment_nav(); ?>
        <h3>Comments</h3>
        <div class="comments-area">
        
        <ul class="media-list">
        <?php wp_list_comments('callback=proui_comments'); ?>

    </ul><!-- .comment-list -->
</div>
        <?php //twentyfifteen_comment_nav(); ?>

    <?php endif; // have_comments() ?>

    <?php
        // If comments are closed and there are comments, let's leave a little note, shall we?
        if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
    ?>
        <p class="no-comments"><?php _e( 'Comments are closed.', 'twentyfifteen' ); ?></p>
    <?php endif; ?>

    <?php comment_form_new(); ?>

</div><!-- .comments-area -->

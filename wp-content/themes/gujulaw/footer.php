<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */
global $shortnametheme;
?>
<!--Footer Start Here-->
        <footer class="footer">
            <div class="container">
                <div class="footer_sec">
                    <div class="footer_product">
                          <?php wp_nav_menu( array('menu' => 'Footer menu 1',  'menu_class' => 'footer_menu')); ?>
                    </div>
                    <ul class="footer_address_block">    
                		<li>
                            <div class="country_name"> 
                                <p><?php echo stripslashes(get_option($shortnametheme."_footer_name")); ?></p>
                            </div>

	                        <div class="company_address"> 
	                            <p><?php echo stripslashes(get_option($shortnametheme."_address")); ?></p>
	                        </div>
	                        
	                        <div class="company_call">
	                            <p><?php echo stripslashes(get_option($shortnametheme."_footer_phn")); ?></p>
	                        </div>
                    	</li>

                        <!--<li>
                            <div class="country_name"> 
                                <p><?php echo stripslashes(get_option($shortnametheme."_footer_name2")); ?></p>
                            </div>

                            <div class="company_address"> 
                                <p><?php echo stripslashes(get_option($shortnametheme."_address2")); ?></p>
                            </div>
                            
                            <div class="company_call">
                                <p><?php echo stripslashes(get_option($shortnametheme."_footer_phn2")); ?></p>
                            </div>
                        </li>-->

                        <li>
                            <div class="country_name"> 
                                <p><?php echo stripslashes(get_option($shortnametheme."_footer_name3")); ?></p>
                            </div>

                            <div class="company_address"> 
                                <p><?php echo stripslashes(get_option($shortnametheme."_address3")); ?></p>
                            </div>
                            
                            <div class="company_call">
                                <p><?php echo stripslashes(get_option($shortnametheme."_footer_phn3")); ?></p>
                            </div>
                        </li>

           			</ul>
                        <div class="social_icons">
                            <ul>
							   <li><a href="<?php echo stripslashes(get_option($shortnametheme."_fblink")); ?>" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/twitter.png" alt="Twitter" title="Twitter"></a></li>
                                <li><a href="<?php echo stripslashes(get_option($shortnametheme."_twlink")); ?>" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/facebook.png" alt="Facebook" title="Facebook"></a></li>
                             
                                <li><a href="<?php echo stripslashes(get_option($shortnametheme."_lilink")); ?>" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/linkedin.png" alt="Linkedin" title="Linkedin"></a></li>
                            </ul> 
                        </div>
                </div>
                <div class="copyright">Copyright  &copy; <?php echo date('Y'); ?> The Guju Law Firm. All Rights Reserved.</div>
                
            </div>
        </footer>

</div>
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.mmenu.min.all.js"></script>
        <script type="text/javascript">
        $(function() {
        $('nav#menu').mmenu({
        extensions : [ 'effect-slide-menu', 'pageshadow' ],
        searchfield : false,
        counters : false,
        navbar : {title : 'Menu'},
        navbars : [
        {
            
        position : 'top',
        content : [
        'prev',
        'title',
        'close'
        ]
        }
        ]
        });
        });
</script>
<script type="text/javascript">
            $(window).scroll(function() {
            if ($(this).scrollTop() > 1){  
                $('.header').addClass("sticky");
				
              }
              else{
                $('.header').removeClass("sticky");
				
              }
            });
 </script>

<script type="text/javascript">
            $(window).scroll(function() {
            if ($(this).scrollTop() > 1){  
                $('.inner-header').addClass("sticky");
				$('.inner-header .logo img').attr('src', '<?php echo get_template_directory_uri(); ?>/images/logo.png');
				
				
              }
              else{
                $('.inner-header').removeClass("sticky");
				$('.inner-header .logo img').attr('src', '<?php echo get_template_directory_uri(); ?>/images/inner-logo.png');
			
              }
            });
 </script>

<script>
          document.addEventListener( 'wpcf7mailsent', function( event ) {
            window.location = "<?php echo site_url(); ?>/thank-you";
        }, false );
        </script>


<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.min.js"></script>   
<script type="text/javascript">
        $(document).ready(function() { 
          $('.event_list').owlCarousel({
            margin:12,
            loop:true,
            items:6,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                 414:{
                   items:2,
                   nav:true,
                   margin: 8,
                   loop:true
           },
                568:{
                   items:2,
                   nav:true,
                   margin: 8,
                   loop:true
           },
                640:{
                   items:3,
                   nav:true,
                   margin: 10,
                   loop:true
           },
                768:{
                   items:3,
                   nav:true,
                   margin: 8,
                   loop:true
           },
                1024:{
                   items:3,
                   nav:true,
                   margin: 8,
                   loop:true
           },
                1260:{
                   items:3,
                   nav:true,
                   margin: 9,
                   loop:true, 
               },
              
                1600:{
                    items:3,
                    nav:true,
                    loop:true,    
                }
            }
       })  
            
    });
        </script>
        
         <script>            
            $(function() {  
                $('.header_right .search').click(function(){              
                  $(".header-searchform-wrap").slideToggle(300); 
                });
                $('.search-close-btn').click(function(){
                  $(".header-searchform-wrap").slideToggle(300); 
                });
				
			
            }); 

        </script>
      
</body>
</html>

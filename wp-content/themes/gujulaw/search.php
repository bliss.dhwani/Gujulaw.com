<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<section class="inner-banner" style="background: url('<?php the_field("inner-banner", '86'); ?>') no-repeat center top; height: 520px; background-size: cover;">
<div class="banner-wrap">
<div class="inner-banner-con">
<div class="banner-con-wrap">
<?php $banner_logo = get_field('inner_banner_logo'); 
if($banner_logo != Null){
?>
<figure><img src="<?php echo $banner_logo['url']; ?>"  alt="<?php echo $banner_logo['alt']; ?>" title="<?php echo $banner_logo['title']; ?>"/></figure>
<?php } ?>
<?php the_field("inner_banner_content"); ?>
</div>
</div>
</div>
</section>

<section class="search-page blog">
<div class="container">
<h1 class="page-title"><?php printf( __( 'Search Results for: %s', '' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
		<?php if ( have_posts() ) :
			while ( have_posts() ) : the_post(); ?>
            <div class="blog-block" id="post-<?php the_ID(); ?>">

        <div class="blog-txt-wrap">
        <h2><a href="<?php the_permalink() ?>"><?php the_title( ); ?></a></h2>
       
        <?php $output = get_the_excerpt(); if($output != '') {?>
        <div class="blog-txt"><p><?php the_excerpt(); ?></p></div>
        <?php } ?>
        <div class="blog-btm"><a class="btn btn-primary btn-green blog-readmore" href="<?php the_permalink() ?>">Read More</a>
        </div>
        </div>
        </div>
			<?php endwhile; ?>
		 <?php if(paginate_links()) { ?>
    <div class="pagination">
     <?php wp_paginate(); ?>
     </div>
     <?php } ?>

		<?php else : ?>

			<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?></p>
	<?php endif; ?>

</div>
</section>

<?php get_footer();

<?php
/**
 * Template Name: contact Page
 *
 **/
get_header(); ?>
<div class="inner_banner" style="background: url('<?php the_field('inner-banner', '86');?>') no-repeat center top; height: 520px; background-size: cover;">
</div>

    <section class="inner-sec">
        <div class="container">
            <div class="inner-page">
               <h1><?php the_title(); ?></h1>
					<div class="contact-numebr">
						
						<h2> <span> <strong>Email: </strong></span><?php the_field('email'); ?></h2>

						<h2> <span> <strong>Phone: </strong></span><?php the_field('phone_number'); ?></h2>
					</div>
				<div class="inner-contact-sec">
				
				 <?php if( have_rows('offices') ): ?>
					<ul>
						<?php while( have_rows('offices') ): the_row(); ?>
						<li>
									<div class="inner-contact-lft"> 
										<?php the_sub_field('address'); ?>
									</div>
									<div class="inner-contact-rgt"> 
											<?php the_sub_field('map'); ?>
									</div>
						
						</li>
					<?php endwhile; ?>
					</ul>
				<?php endif; ?>

					
				</div>
            </div>
        </div>
    </section>



<?php get_footer(); ?>
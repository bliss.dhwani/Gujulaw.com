<?php
/**
 * Template Name: Practice Areas Page
 *
 **/
get_header(); ?>
<div class="inner_banner" style="background: url('<?php the_field('inner-banner', '86');?>') no-repeat center top; height: 520px; background-size: cover;">
</div>

    <section class="inner-sec">
        <div class="container">
            <div class="inner-page">
               <h1><?php the_title(); ?></h1>
					  
						<?php if ( have_posts() ) :
while ( have_posts() ) : the_post();
the_content();
endwhile;
endif; ?>			
									
				
				<div class="inner-practice-sec">
					
				 <?php if( have_rows('practice_areas') ): ?>
					
					<div class="practice-main">
						<?php while( have_rows('practice_areas') ): the_row(); ?>
						
						<div class="practice-box">			
						
									 <h3><?php the_sub_field('practice_areas_title'); ?></h3>
								
									<div class="practice-text"> 
											<?php the_sub_field('practice_areas_text'); ?>
									</div>
						
						</div>
					<?php endwhile; ?>
					</div>
				<?php endif; ?>

					
				</div>
            </div>
        </div>
    </section>



<?php get_footer(); ?>
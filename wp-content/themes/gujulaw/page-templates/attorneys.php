<?php
/**
 * Template Name: Attorneys Page
 *
 **/
get_header(); ?>
<div class="inner_banner" style="background: url('<?php the_field('inner-banner', '86');?>') no-repeat center top; height: 520px; background-size: cover;">
</div>

    <section class="inner-sec">
        <div class="container">
            <div class="inner-page">
               <h1><?php the_title(); ?></h1>
					 <?php if ( have_posts() ) :
while ( have_posts() ) : the_post();
the_content();
endwhile;
endif; ?>
				<div class="inner-about-sec">
				 <?php if( have_rows('attorneys') ): ?>
					<ul>
						<?php while( have_rows('attorneys') ): the_row();
						// vars
								$image = get_sub_field('attorney__image');
						?>
						
						<li>
									<div class="attorney-img"> 
										<figure><span><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></span></figure>
									</div>
									 <h4><?php the_sub_field('attorneys_name'); ?></h4>
									
									<div class="attorney-text"> 
											<?php the_sub_field('attorney_description'); ?>
									</div>
						
						</li>
					<?php endwhile; ?>
					</ul>
				<?php endif; ?>

					
				</div>
            </div>
        </div>
    </section>



<?php get_footer(); ?>
<?php
/**
 * Template Name: About Page
 *
 **/
get_header(); ?>
<div class="inner_banner" style="background: url('<?php the_field('inner-banner', '86');?>') no-repeat center top; height: 520px; background-size: cover;">
</div>

    <section class="inner-sec">
        <div class="container">
            <div class="inner-page about">
               <h1>About The Guju Law Firm</h1>
					 <?php if ( have_posts() ) :
while ( have_posts() ) : the_post();
the_content();
endwhile;
endif; ?>
            </div>
        </div>
    </section>



<?php get_footer(); ?>
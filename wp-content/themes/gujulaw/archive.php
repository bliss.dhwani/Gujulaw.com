<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="inner_banner" style="background: url('<?php the_field('inner-banner', '86');?>') no-repeat center top; height: 520px; background-size: cover;">
</div>
<section class="slider">
<?php do_shortcode('[slider]'); ?>
</section>

<div class="inner-con">
<div class="container">
<?php the_archive_title( '<h1 class="page-heading">', '</h1>' ); ?>
<div class="content-lft">

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="blog-block" id="post-<?php the_ID(); ?>">
        <div class="date-area">
        <div class="date"><?php the_time('j'); ?></div>
        <div class="month"><?php the_time('M'); ?></div>
        </div>
        <div class="blog-txt-wrap">
        <h2><a href="<?php the_permalink() ?>"><?php the_title( ); ?></a></h2>
        <div class="blog-top">
        
        <span class="postedby"><strong>Posted by:</strong> <?php the_author_nickname(); ?></span>
        <?php the_tags( '<span class="tags">Tags: ' , ', ', '</span>' ); ?>
        <span class="num-of-commemts"><a href="<?php comments_link(); ?>"><?php comments_number('0 Comment', '1 Comment', '% Comments' );?></a></span>
        </div>
        
        <?php $img= get_the_post_thumbnail($post_id, 'full'); ?>
        
        <div class="blog-txt"><?php echo $img; ?><p><?php the_excerpt(); ?></p></div>
        <div class="blog-btm"><div class="blog-readmore"><a href="<?php the_permalink() ?>">Read More</a></div>
        </div>
        </div>
        </div>
    <?php endwhile; ?>
    <?php if(paginate_links()) {?>
    <div class="pagination">
     <?php wp_paginate(); ?>
     </div>
     <?php } ?>
    <?php else :
                //    get_template_part( 'content', 'none' );
endif; ?>
</div>

<div class="content-rgt">

<div class="blog-rgt-section newsletter">
<h3>Newsletter</h3>
<?php echo do_shortcode('[mailpoet_form id="1"]');?>
</div>

<div class="blog-rgt-section archives">
<h3>Archives</h3>
<?php dynamic_sidebar('archives'); ?>
</div>

<div class="blog-rgt-section recent">
<h3>Recent Post</h3>
<?php dynamic_sidebar('recent_post'); ?>
</div>

</div>

</div>
</div>

<?php get_footer();

<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content = "telephone=no">
<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1.0">
<link rel="icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/fav.ico" type="images/ico">
<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/mediaquery.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery.mmenu.all.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/owl.carousel.min.css">
<!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/css3-mediaqueries.js"  type="text/javascript"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5shiv.js"  type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>
</head>
<?php global $more; 
	global $shortnametheme;
	$shortnametheme = "gl";
?>

<body <?php body_class(); ?>>
<div id="wrapper">
	<!--Header Start Here-->
	<header class="header <?php if (!is_front_page()){?>inner-header<?php }?>">	
        <div class="header_sec">
		    <div class="inner-container">
			          <div class="mobile_menu">
                            <?php wp_nav_menu( array('menu' => 'Header Menu',  'menu_class' => '', 'container' => 'nav', 'container_id' => 'menu')); ?>
                            
						   <a class="menu-btn" href="#menu">Menu </a>
                      </div>
                    <div class="header_left">
					
                        <div class="logo">
                            <a href="<?php echo site_url(); ?>/">
                                <?php if (is_front_page()){?>
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png" alt="Logo" title="Gujulaw">
                                <?php } else { ?>
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/inner-logo.png" alt="Logo" title="Gujulaw">
                                <?php } ?>
                            </a>
                        </div>
                    </div>
                    <div class="header_right">
                        <div class="desktop-nav">
                            <?php wp_nav_menu( array('menu' => 'Header Menu',  'menu_class' => '', 'container' => 'nav', 'container_id' => 'cssmenu')); ?>
                        </div>
                        
                        <div class="search">
                            <?php if (is_front_page()){?>
								<div class="search-btn main-search"></div>  
                            <?php } else { ?>
								<div class="search-btn inner-search"></div>  
                             <?php } ?> 
                        </div>
                        <div id="searchform" class="header-searchform-wrap">
                            <div class="container">
                                <?php dynamic_sidebar('header_search'); ?>
                                <span id="searchform-close" class="search-close-btn"><img  src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/cancel_logo.png" alt="Close Button" title="Close Button"/></span>
                            </div>
                        </div>
                    </div>
			    </div>
		</div>
	</header>
    
	

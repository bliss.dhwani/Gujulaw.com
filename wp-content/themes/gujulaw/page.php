<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="inner_banner" style="background: url('<?php the_field('inner-banner', '86');?>') no-repeat center top; height: 520px; background-size: cover;">
</div>
<section class="inner-sec">
        <div class="container">
            <div class="inner-page">
                <h1><?php the_title(); ?></h1>
                <?php if ( have_posts() ) :
while ( have_posts() ) : the_post();
the_content();
endwhile;
endif; ?>
            </div>
        </div>
    </section>

<?php get_footer();
